# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* this repository is abou the sumnima's website. It has got the selection tab of About me, contact me and calculation.
* the Contact would allow the user to send me an email. 
* The calculation would allow the user to input two numbers and then get the sum of two numbers.
* [Learn Markdown](https://bitbucket.org/sumnimarana1/a02rana)

# Uses

- jQuery, a JavaScript library that simplifies client-side scripting
- BootStrap, a framework for responsive web design 
- JavaScript, a scripting language for the web
- HTML, a markup language for web apps
- CSS, a style sheet language for styling markup languages such as html

# Getting Started

Visual Studio Code is recommended, but you can use Sublime, Notepad++, or any text editor to modify the code. 

- Right-click on M04 folder / "Open with Code".
- In VS Code, format code with SHIFT + ALT + f and CTRL-S to save.
- Right-click to run tests

### Contribution guidelines ###

* Writing tests- Test for user different input values, with null value, with invalid numbers like positive or decimal values, No negative numbers. 
* Code review- Written in Javascript and html with the css styling
* Other guidelines- run also the tests to see the validation of the code.
### Who do I talk to? ###

* Repo owner or admin(https://bitbucket.org/sumnimarana1)
